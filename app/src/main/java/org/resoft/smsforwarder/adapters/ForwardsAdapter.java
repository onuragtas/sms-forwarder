package org.resoft.smsforwarder.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.resoft.smsforwarder.R;
import org.resoft.smsforwarder.entities.Forward;
import org.resoft.smsforwarder.models.Sms;

import java.util.List;

public class ForwardsAdapter extends RecyclerView.Adapter<ForwardsAdapter.ViewHolder> {
    private List<Forward> items;
    private final OnItemClickListener listener;

    public ForwardsAdapter(OnItemClickListener listener, List<Forward> items) {
        this.listener = listener;
        this.items = items;
    }

    @Override
    public ForwardsAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.forward_list_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.name.setText(items.get(i).getTitle());
        viewHolder.sms.setText(items.get(i).getMessage());
        viewHolder.sendedTo.setText(items.get(i).getTo());
        viewHolder.bind(items.get(i), listener);
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public void setData(List<Forward> data) {
        items = data;
        notifyDataSetChanged();
    }

    public void addItem(Forward item) {
        items.add(item);
        notifyItemInserted(items.size());
    }

    public void removeItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, items.size());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, sms, sendedTo;

        public ViewHolder(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.name);
            sms = (TextView) view.findViewById(R.id.message);
            sendedTo = (TextView) view.findViewById(R.id.sendedTo);
        }

        public void bind(final Forward item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(Forward item);
    }

}