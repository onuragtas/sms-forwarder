package org.resoft.smsforwarder.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.resoft.smsforwarder.DataManager;
import org.resoft.smsforwarder.R;
import org.resoft.smsforwarder.entities.Rule;
import org.resoft.smsforwarder.enums.SmsEnum;
import org.resoft.smsforwarder.enums.TopicEnum;

import java.util.List;

public class RulesAdapter extends RecyclerView.Adapter<RulesAdapter.ViewHolder> {
    private List<Rule> items;
    private final OnItemClickListener listener;

    public RulesAdapter(OnItemClickListener listener, List<Rule> items) {
        this.listener = listener;
        this.items = items;
    }

    @Override
    public RulesAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rule_list_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        SmsEnum type = SmsEnum.getReverse(items.get(i).getType());
        TopicEnum topic = TopicEnum.getReverse(items.get(i).getTopic());

        viewHolder.type.setText(type.new Entry(DataManager.activity).toString());
        viewHolder.topic.setText(topic.new Entry(DataManager.activity).toString());

        viewHolder.value.setText(items.get(i).getValue());
        viewHolder.to.setText(items.get(i).getTo());
        viewHolder.bind(items.get(i), listener);
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public void setData(List<Rule> item) {
        items = item;
        notifyDataSetChanged();
    }

    public void addItem(Rule item) {
        items.add(item);
        notifyItemInserted(items.size());
    }

    public void removeItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, items.size());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView type, topic, value, to;

        public ViewHolder(View view) {
            super(view);

            type = view.findViewById(R.id.type);
            topic = view.findViewById(R.id.topic);
            value = view.findViewById(R.id.value);
            to = view.findViewById(R.id.sendTo);
        }

        public void bind(final Rule item, final OnItemClickListener listener) {
            itemView.setOnClickListener(v -> listener.onItemClick(item));
            itemView.setOnLongClickListener(v -> {
                listener.OnLongItemClick(item);
                return false;
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Rule item);
        void OnLongItemClick(Rule item);
    }

}