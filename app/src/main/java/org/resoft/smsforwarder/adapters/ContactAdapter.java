package org.resoft.smsforwarder.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import org.resoft.smsforwarder.R;
import org.resoft.smsforwarder.entities.Contact;

import java.util.ArrayList;

public class ContactAdapter extends BaseAdapter implements Filterable {

    private ArrayList<Contact> originalData = null;
    private ArrayList<Contact>filteredData = null;
    private LayoutInflater mInflater;
    private ItemFilter mFilter = new ItemFilter();

    public ContactAdapter(Context context, ArrayList<Contact> data) {
        this.filteredData = data ;
        this.originalData = data ;
        mInflater = LayoutInflater.from(context);
    }

    public int getCount() {
        if (filteredData == null){
            return 0;
        }
        return filteredData.size();
    }

    public Object getItem(int position) {
        return filteredData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        // A ViewHolder keeps references to children views to avoid unnecessary calls
        // to findViewById() on each row.
        ViewHolder holder;

        // When convertView is not null, we can reuse it directly, there is no need
        // to reinflate it. We only inflate a new View when the convertView supplied
        // by ListView is null.
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_contact, null);

            // Creates a ViewHolder and store references to the two children views
            // we want to bind data to.
            holder = new ViewHolder();
            holder.text = (TextView) convertView.findViewById(R.id.name);

            // Bind the data efficiently with the holder.

            convertView.setTag(holder);
        } else {
            // Get the ViewHolder back to get fast access to the TextView
            // and the ImageView.
            holder = (ViewHolder) convertView.getTag();
        }

        // If weren't re-ordering this you could rely on what you set last time
        holder.text.setText(filteredData.get(position).getName());

        return convertView;
    }

    public void setData(ArrayList<Contact> list) {
        this.filteredData = list;
        this.originalData = list;
        notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView text;
    }

    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final ArrayList<Contact> list = originalData;

            int count = list.size();
            final ArrayList<Contact> nlist = new ArrayList<Contact>(count);

            String filterableString ;
            String filterableStringNumber ;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i).getName();
                filterableStringNumber = list.get(i).getNumber();
                if (filterableString.toLowerCase().contains(filterString) || filterableStringNumber.toLowerCase().contains(filterString)) {
                    nlist.add(new Contact(list.get(i).getName(), list.get(i).getNumber()));
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<Contact>) results.values;
            notifyDataSetChanged();
        }

    }
}