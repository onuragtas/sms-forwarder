package org.resoft.smsforwarder.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import org.resoft.smsforwarder.R;
import org.resoft.smsforwarder.models.Sms;

import java.util.ArrayList;
import java.util.List;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ViewHolder> {
    private List<Sms> smses;
    private final OnItemClickListener listener;

    public MessagesAdapter(OnItemClickListener listener, List<Sms> smses) {
        this.listener = listener;
        this.smses = smses;
    }

    @Override
    public MessagesAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.message_list_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.name.setText(smses.get(i).getAddress());
        viewHolder.sms.setText(smses.get(i).getBody());
        viewHolder.bind(smses.get(i), listener);
    }

    @Override
    public int getItemCount() {
        if (smses == null) {
            return 0;
        }
        return smses.size();
    }

    public void setData(List<Sms> sms) {
        smses = sms;
        notifyDataSetChanged();
    }

    public void addItem(Sms sms) {
        smses.add(sms);
        notifyItemInserted(smses.size());
    }

    public void removeItem(int position) {
        smses.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, smses.size());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, sms;

        public ViewHolder(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.name);
            sms = (TextView) view.findViewById(R.id.message);
        }

        public void bind(final Sms item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(Sms sms);
    }

}