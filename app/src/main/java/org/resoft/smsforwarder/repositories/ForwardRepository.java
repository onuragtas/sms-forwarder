package org.resoft.smsforwarder.repositories;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import org.resoft.smsforwarder.entities.Forward;
import org.resoft.smsforwarder.entities.Rule;

import java.util.List;

@Dao
public interface ForwardRepository {

    @Insert
    long add(Forward obj);

    @Query("SELECT * FROM forwards")
    List<Forward> list();

    @Query("DELETE FROM forwards WHERE id=:id")
    void delete(Long id);
}
