package org.resoft.smsforwarder.repositories;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import org.resoft.smsforwarder.entities.Rule;

import java.util.List;

@Dao
public interface RuleRepository {

    @Insert
    long add(Rule obj);

    @Query("SELECT * FROM rules")
    List<Rule> list();

    @Query("SELECT * FROM rules WHERE type = :type AND topic = :topic AND value = :value AND `to` = :to")
    List<Rule> list(String type, String topic, String value, String to);

    @Query("DELETE FROM rules WHERE id=:id")
    void delete(Long id);
}
