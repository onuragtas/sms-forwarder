package org.resoft.smsforwarder.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.sql.Date;

@Entity(tableName = "forwards")
public class Forward {

    @PrimaryKey(autoGenerate = true)
    private Long id;

    @ColumnInfo(name = "to")
    private String to = null;

    @ColumnInfo(name = "title")
    private String title = null;

    @ColumnInfo(name = "message")
    private String message = null;

    @ColumnInfo(name = "created_at")
    private Date created_at = new Date(System.currentTimeMillis());

    public Forward(String to, String title, String message) {
        this.to = to;
        this.title = title;
        this.message = message;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }
}
