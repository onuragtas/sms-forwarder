package org.resoft.smsforwarder.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.sql.Date;


@Entity(tableName = "rules")
public class Rule {

    @PrimaryKey(autoGenerate = true)
    private Long id;

    @ColumnInfo(name = "type")
    private String type = null;

    @ColumnInfo(name = "topic")
    private String topic = null;

    @ColumnInfo(name = "value")
    private String value = null;

    @ColumnInfo(name = "to")
    private String to = null;

    @ColumnInfo(name = "created_at")
    private Date created_at = new Date(System.currentTimeMillis());

    public Rule(String type, String topic, String value, String to) {
        this.type = type;
        this.topic = topic;
        this.value = value;
        this.to = to;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }
}
