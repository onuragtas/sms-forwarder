package org.resoft.smsforwarder;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.resoft.smsforwarder.models.Sms;
import org.resoft.smsforwarder.providers.ContactProvider;
import org.resoft.smsforwarder.providers.SmsProvider;
import org.resoft.smsforwarder.utils.Permissions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_DEFAULT_APP = 11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Permissions.request(MainActivity.this);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    (new ContactList()).start();
                    (new SmsList()).start();
                    initialize();
                } else {
                    Permissions.request(MainActivity.this);
                }
                return;
            }
        }
    }

    private void initialize(){
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
                System.out.println("test");
            }
        });

        AdView mAdView;
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    class ContactList extends Thread {
        @Override
        public void run() {
            super.run();

            ContactProvider provider = new ContactProvider(getApplicationContext());
            provider.setUri(ContactsContract.Contacts.CONTENT_URI);
            provider.setOrder("display_name");
            provider.setOrderType("ASC");
            DataManager.contactList = provider.query();
        }
    }


    class SmsList extends Thread {
        @Override
        public void run() {
            super.run();

            SmsProvider providerSms = new SmsProvider(getApplicationContext());

            providerSms.setOrder("_id");
            providerSms.setOrderType("DESC");
            providerSms.setUri("content://sms/conversations/");
            providerSms.setGroup("thread_id");

            ArrayList<Sms> smsList = new ArrayList<>();
            ArrayList<Sms> data = providerSms.query();

            for (Sms item:data) {
                if (!item.getAddress().equals("") && !item.getBody().equals("")){
                    smsList.add(item);
                }
            }
            DataManager.smsList = smsList;
        }
    }
}