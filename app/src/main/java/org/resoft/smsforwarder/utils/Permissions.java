package org.resoft.smsforwarder.utils;

import android.Manifest;
import android.app.Activity;

import androidx.core.app.ActivityCompat;

public class Permissions {

    private static String[] requests = new String[]{
            Manifest.permission.READ_SMS,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.SEND_SMS,
            Manifest.permission.GET_ACCOUNTS,
            Manifest.permission.BROADCAST_SMS,
            Manifest.permission.READ_CONTACTS,
    };

    public static void request(Activity activity) {
        ActivityCompat.requestPermissions(activity, requests, 1);
    }

    public static void requestPermission(Activity activity, String[] permissions, int code) {
        ActivityCompat.requestPermissions(activity, permissions, code);
    }

}
