package org.resoft.smsforwarder.utils;

import android.app.Activity;
import android.app.ProgressDialog;

import androidx.appcompat.app.AlertDialog;

public class Dialogs {

    public static void makeQuestionDialog(Activity mActivity, String title, String message, boolean cancalable, String positiveBtnText, String negativeBtnText, final IDialog cb, Object object){
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle(title);
        if(!message.equals("")) {
            builder.setMessage(message);
        }
        builder.setCancelable(cancalable);

        builder.setPositiveButton(positiveBtnText, (dialog, which) -> cb.onPositive(object));


        builder.setNegativeButton(negativeBtnText, (dialog, id) -> cb.onNegative());
        builder.show();
    }


    public static void progressDialog(Activity activity, String title, String message, boolean cancalable, IDialog cb){
        ProgressDialog pd = new ProgressDialog(activity);
        pd.setTitle(title);
        if (!message.equals("")) {
            pd.setMessage(message);
        }
        pd.show();

        cb.onCallback(pd);
    }

    public interface IDialog {
        void onPositive(Object object);
        void onNegative();
        void onCallback(ProgressDialog pd);
    }
}

