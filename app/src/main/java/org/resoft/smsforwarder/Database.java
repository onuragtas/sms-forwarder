package org.resoft.smsforwarder;

import android.app.Notification;
import android.content.Context;
import android.media.Image;

import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import org.resoft.smsforwarder.converters.Converters;
import org.resoft.smsforwarder.entities.Forward;
import org.resoft.smsforwarder.entities.Rule;
import org.resoft.smsforwarder.repositories.ForwardRepository;
import org.resoft.smsforwarder.repositories.RuleRepository;

import java.io.File;


@androidx.room.Database(entities = {Rule.class, Forward.class}, version = 2)
@TypeConverters({Converters.class})
public abstract class Database extends RoomDatabase {

    private static  Database INSTANCE;

    public abstract RuleRepository rule();
    public abstract ForwardRepository forward();

    public static Database getDatabase(Context context){
        if(INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), Database.class, "sms-forwarder").allowMainThreadQueries().fallbackToDestructiveMigration().build();
        }
        return  INSTANCE;
    }

    public  static  void destroyInstance(){
        INSTANCE = null;
    }

}
