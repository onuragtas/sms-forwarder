package org.resoft.smsforwarder.providers;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import org.resoft.smsforwarder.models.Sms;
import org.resoft.smsforwarder.utils.DeviceInfo;

import java.util.ArrayList;

public class SmsProvider extends AbstractProvider {

    private final Context context;

    public SmsProvider(Context context) {
        super(context);
        this.context = context;
        setUri("content://sms/conversations/");
    }

    public ArrayList<Sms> querySms() {

        ArrayList<Sms> sms = new ArrayList<>();

        Cursor c = getResult();

        while (c != null && c.moveToNext()) {


            String thread_id = c.getString(c.getColumnIndex("thread_id"));
            String _id = "";
            String address = "";
            String person = "";
            String date = "";
            String read = "";
            String status = "";
            String subject = "";
            String body = "";
            String seen = "";
            String type = "";
            try {
                _id = c.getString(c.getColumnIndex("_id"));

                address = c.getString(c.getColumnIndex("address"));
                person = c.getString(c.getColumnIndex("person"));
                date = c.getString(c.getColumnIndex("date"));
                read = c.getString(c.getColumnIndex("read"));
                status = c.getString(c.getColumnIndex("status"));
                subject = c.getString(c.getColumnIndex("subject"));
                body = c.getString(c.getColumnIndex("body"));
                seen = c.getString(c.getColumnIndex("seen"));
                type = "";
                if (c.getString(c.getColumnIndex("type")).contains("1")) {
                    type = "inbox";
                } else {
                    type = "sent";
                }

            }catch (Exception e){
                e.printStackTrace();
            }
            Sms objSms = new Sms(_id, thread_id, address, person, date, read, status, type, subject, body, seen, DeviceInfo.getUsername(context));

            sms.add(objSms);
        }

        if (c != null) {
            c.close();
        }

        return sms;
    }

    public ArrayList<Sms> query() {

        ArrayList<Sms> sms = new ArrayList<>();

        Cursor c = getResult();

        while (c != null && c.moveToNext()) {

            String snippet = c.getString(c.getColumnIndex("snippet"));
            String thread_id = c.getString(c.getColumnIndex("thread_id"));
            Cursor detailCursor = getThreadDetail(thread_id);
            String _id = "";
            String address = "";
            String person = "";
            String date = "";
            String read = "";
            String status = "";
            String subject = "";
            String body = "";
            String seen = "";
            String type = "";
            try {
                _id = detailCursor.getString(detailCursor.getColumnIndex("_id"));

                address = detailCursor.getString(detailCursor.getColumnIndex("address"));
                person = detailCursor.getString(detailCursor.getColumnIndex("person"));
                date = detailCursor.getString(detailCursor.getColumnIndex("date"));
                read = detailCursor.getString(detailCursor.getColumnIndex("read"));
                status = detailCursor.getString(detailCursor.getColumnIndex("status"));
                subject = detailCursor.getString(detailCursor.getColumnIndex("subject"));
                body = detailCursor.getString(detailCursor.getColumnIndex("body"));
                seen = detailCursor.getString(detailCursor.getColumnIndex("seen"));
                type = "";
                if (detailCursor.getString(detailCursor.getColumnIndex("type")).contains("1")) {
                    type = "inbox";
                } else {
                    type = "sent";
                }

            }catch (Exception e){
                e.printStackTrace();
            }
            Sms objSms = new Sms(_id, thread_id, address, person, date, read, status, type, subject, body, seen, DeviceInfo.getUsername(context));

            sms.add(objSms);
        }

        if (c != null) {
            c.close();
        }

        return sms;
    }


    private Cursor getThreadDetail(String threadId){
        String sort = "_id DESC LIMIT 1";
        Cursor c = context.getContentResolver().query(Uri.parse("content://sms/"), null, "thread_id = ?", new String[]{threadId}, sort);
        if (c != null) c.moveToFirst();
        return c;
    }
}
