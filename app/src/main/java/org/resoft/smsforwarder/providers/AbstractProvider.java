package org.resoft.smsforwarder.providers;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import org.resoft.smsforwarder.models.Sms;

import java.util.ArrayList;

public abstract class AbstractProvider {

    private final Context context;

    public AbstractProvider(Context context) {
        this.context = context;
    }

    private int limit = 50;
    private int page = -1;
    private String order = "", orderType = "";
    private String group = "";
    private String uri;
    private Uri URI = null;
    private ArrayList<String[]> where = new ArrayList<>();

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public void setUri(Uri uri){
        this.URI = uri;
    }

    public void addWhere(String[] item){
        where.add(item);
    }

    public void setWhere(ArrayList<String[]> items){
        where = items;
    }

    public ArrayList<String[]> getWhere() {
        return where;
    }

    public Cursor getResult() {
        String sort = "";

        String[] projection = null;

        if (!getOrder().equals("") && !getOrderType().equals("")) {
            sort += getOrder() + " " + getOrderType() + " ";
        }

        if (getPage() > -1){
            if (getOrder().equals("") && getOrderType().equals("")) {
                sort += "_id DESC ";
            }
            sort += "LIMIT 0, " + (getLimit() + page) + " ";
        }

        String whereQuery = null;
        String[] whereValues = null;

        if (getWhere().size() > 0){

            whereQuery = "";
            whereValues = new String[where.size()];

            ArrayList<String[]> list = getWhere();

            int i = 0;
            for (String[] item: list) {

                whereQuery += item[0]+" "+item[1]+" ? ";
                whereValues[i] = item[2];

                i++;
            }

        }

        ArrayList<Sms> sms = new ArrayList<>();
        if(URI == null) {
            URI = Uri.parse(getUri());
        }


        Cursor c = context.getContentResolver().query(URI, projection, whereQuery, whereValues, sort);

        return c;
    }
}
