package org.resoft.smsforwarder.providers;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;


import org.resoft.smsforwarder.entities.Contact;
import org.resoft.smsforwarder.utils.DeviceInfo;

import java.util.ArrayList;

public class ContactProvider extends AbstractProvider {

    private final Context context;

    public ContactProvider(Context context) {
        super(context);
        this.context = context;
        setUri(ContactsContract.Contacts.CONTENT_URI);
    }

    public ArrayList<Contact> query() {

        ArrayList<Contact> data = new ArrayList<>();

        ContentResolver cr = this.context.getContentResolver();
        Cursor cur = getResult();

        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));

                String number = "";

                if (cur.getInt(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        number = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    }
                    pCur.close();
                }

                Contact contact = new Contact(name, number);
                data.add(contact);
            }
        }

        if (cur != null) {
            cur.close();
        }

        return data;
    }
}

