package org.resoft.smsforwarder.receivers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import org.resoft.smsforwarder.DataManager;
import org.resoft.smsforwarder.Database;
import org.resoft.smsforwarder.entities.Forward;
import org.resoft.smsforwarder.entities.Rule;
import org.resoft.smsforwarder.enums.SmsEnum;
import org.resoft.smsforwarder.enums.TopicEnum;
import org.resoft.smsforwarder.repositories.ForwardRepository;
import org.resoft.smsforwarder.repositories.RuleRepository;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;

public class SmsReceiver extends BroadcastReceiver {
    public SmsReceiver() {

    }

    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private static final String TAG = "SMSBroadcastReceiver";
    private static final String FORMAT = "From: {from}\n" +
            "Date: {when}\n" +
            "**********\n" +
            "{content}";

    @Override
    public void onReceive(Context context, Intent intent) {
        DataManager.context = context;
        RuleRepository repository = Database.getDatabase(context).rule();
        List<Rule> ruleList = repository.list();

        Log.i(TAG, "Intent recieved: " + intent.getAction());

        if (intent.getAction().equals(SMS_RECEIVED)) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Object[] pdus = (Object[]) bundle.get("pdus");
                final SmsMessage[] messages = new SmsMessage[pdus.length];
                for (int i = 0; i < pdus.length; i++) {
                    messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                }
                if (messages.length > -1) {
                    Log.i(TAG, "Message recieved: " + messages[0].getMessageBody());

                    for (Rule rule : ruleList) {
                        HashMap hashMap = checkRule(rule, messages[0]);

                        if (hashMap.get("send").equals(true)) {
                            forwardSms(messages[0], (Rule) hashMap.get("rule"));
                        }
                    }
                }
            }
        }
    }

    private void forwardSms(SmsMessage message, Rule rule) {
        try {

            ForwardRepository repository = Database.getDatabase(DataManager.context).forward();

            SmsManager smsManager = SmsManager.getDefault();
            String smsBody = FORMAT.replace("{from}", message.getOriginatingAddress());
            smsBody = smsBody.replace("{when}", new Date(System.currentTimeMillis()).toString());
            smsBody = smsBody.replace("{content}", message.getMessageBody());

            smsManager.sendTextMessage(rule.getTo(), null, message.getMessageBody(), null, null);
            Toast.makeText(DataManager.context, "Message Sent", Toast.LENGTH_LONG).show();

            repository.add(new Forward(rule.getTo(), message.getOriginatingAddress(), message.getMessageBody()));

        } catch (Exception ex) {
            Toast.makeText(DataManager.context, ex.getMessage(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    private HashMap checkRule(Rule rule, SmsMessage message) {
        boolean send = false;
        Rule activeRule = null;

        String checkString = "";
        if (rule.getType().equals(SmsEnum.ADDRESS.name())) {
//                checkString = message.getDisplayOriginatingAddress();
            checkString = message.getOriginatingAddress();
        } else if (rule.getType().equals(SmsEnum.CONTENT.name())) {
            checkString = message.getMessageBody();
        }

        if (rule.getTopic().equals(TopicEnum.DOES_CONTAIN_TOPIC.name())) {
            if (checkString.contains(rule.getValue())) {
                send = true;
                activeRule = rule;
            }
        } else if (rule.getTopic().equals(TopicEnum.DOES_NOT_CONTAIN_TOPIC.name())) {
            if (!checkString.contains(rule.getValue())) {
                send = true;
                activeRule = rule;
            }
        } else if (rule.getTopic().equals(TopicEnum.BEGIN_CONTENT_TOPIC.name())) {
            if (checkString.startsWith(rule.getValue())) {
                send = true;
                activeRule = rule;
            }
        } else if (rule.getTopic().equals(TopicEnum.EQUAL_CONTENT_TOPIC.name())) {
            if (checkString.equals(rule.getValue())) {
                send = true;
                activeRule = rule;
            }
        } else if (rule.getTopic().equals(TopicEnum.NOT_EQUAL_CONTENT_TOPIC.name())) {
            if (!checkString.equals(rule.getValue())) {
                send = true;
                activeRule = rule;
            }
        }

        HashMap map = new HashMap();
        map.put("send", send);
        map.put("rule", activeRule);

        return map;
    }
}