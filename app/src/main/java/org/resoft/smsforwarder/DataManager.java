package org.resoft.smsforwarder;

import android.app.Activity;
import android.content.Context;

import org.resoft.smsforwarder.entities.Contact;
import org.resoft.smsforwarder.models.Sms;

import java.util.ArrayList;
import java.util.List;

public class DataManager {
    public static Activity activity;
    public static Context context;
    public static ArrayList<Contact> contactList;
    public static List<Sms> smsList;
}
