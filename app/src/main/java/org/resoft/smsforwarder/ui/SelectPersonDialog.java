package org.resoft.smsforwarder.ui;

import android.app.Activity;
import android.app.Dialog;
import android.os.Build;
import android.os.Handler;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import androidx.annotation.RequiresApi;

import org.resoft.smsforwarder.DataManager;
import org.resoft.smsforwarder.Database;
import org.resoft.smsforwarder.R;
import org.resoft.smsforwarder.adapters.ContactAdapter;
import org.resoft.smsforwarder.entities.Contact;
import org.resoft.smsforwarder.providers.ContactProvider;
import org.resoft.smsforwarder.repositories.RuleRepository;

import java.util.ArrayList;

public class SelectPersonDialog {
    private final Activity activity;
    private ListView listview;
    private final ContactAdapter adapter;
    private ArrayList<Contact> list;
    Dialog customDialog;
    private Button addRuleBtn;
    private EditText searchEditText;


    @RequiresApi(api = Build.VERSION_CODES.N)
    public SelectPersonDialog(Activity activity, ISelectPersonDialog callback) {
        this.activity = activity;
        RuleRepository repository = Database.getDatabase(activity).rule();

        LayoutInflater inflater = (LayoutInflater) activity.getLayoutInflater();
        View customView = inflater.inflate(R.layout.select_person_dialog, null);

        list = DataManager.contactList;

        customDialog = new Dialog(activity, R.style.WideDialog);
        activity.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        customDialog.setContentView(customView);

        initializeUI(customView);

        adapter = new ContactAdapter(activity, list);
        listview.setAdapter(adapter);
        (new List(new Handler())).start();

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                callback.onSelected((Contact) adapter.getItem(position));
                customDialog.dismiss();
            }
        });

        searchEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                System.out.println("Text ["+s+"]");

                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }


    public class List extends Thread {
        private final Handler handler;

        public List(Handler handler){
            this.handler = handler;
        }

        @Override
        public void run() {
            super.run();

            ContactProvider provider = new ContactProvider(activity);
            provider.setUri(ContactsContract.Contacts.CONTENT_URI);
            provider.setOrder("display_name");
            provider.setOrderType("ASC");
            list = provider.query();

            handler.post(() -> adapter.setData(list));

        }

        public ArrayList<Contact> getList(){
            return list;
        }
    }

    private void initializeUI(View customView) {
        addRuleBtn = customView.findViewById(R.id.add_rule);
        searchEditText = customView.findViewById(R.id.search);
        listview = customView.findViewById(R.id.list);
    }

    public void show() {
        customDialog.show();
    }


    public interface ISelectPersonDialog {
        void onSelected(Contact contact);
    }
}
