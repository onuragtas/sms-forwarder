package org.resoft.smsforwarder.ui;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import org.resoft.smsforwarder.Database;
import org.resoft.smsforwarder.R;
import org.resoft.smsforwarder.entities.Contact;
import org.resoft.smsforwarder.entities.Rule;
import org.resoft.smsforwarder.enums.SmsEnum;
import org.resoft.smsforwarder.enums.TopicEnum;
import org.resoft.smsforwarder.models.Sms;
import org.resoft.smsforwarder.repositories.RuleRepository;
import org.resoft.smsforwarder.utils.Permissions;

import java.util.Arrays;
import java.util.List;

public class AddRuleDialog extends AppCompatActivity implements SelectPersonDialog.ISelectPersonDialog {
    private final Activity activity;
    Dialog customDialog;
    private Button addRuleBtn;
    private TextView name;
    private TextView message;
    private Spinner smsItemsList;
    private Spinner topicList;
    private EditText itemEditText;
    private EditText toEditText;
    private TextView logMessage;
    private ImageView selectPersons;


    @RequiresApi(api = Build.VERSION_CODES.N)
    public AddRuleDialog(Activity activity, IDialog callback, Sms sms) {
        this.activity = activity;
        RuleRepository repository = Database.getDatabase(activity).rule();

        LayoutInflater inflater = (LayoutInflater) activity.getLayoutInflater();
        View customView = inflater.inflate(R.layout.add_rule_dialog, null);
        customDialog = new Dialog(activity, R.style.WideDialog);
        activity.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        customDialog.setContentView(customView);

        initializeUI(customView);

        name.setText(sms.getAddress());
        message.setText(sms.getBody());
        itemEditText.setText(sms.getAddress());

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            smsItemsList.setAdapter(new ArrayAdapter(activity, android.R.layout.simple_spinner_dropdown_item, Arrays.stream(SmsEnum.values()).map(item -> item.new Entry(activity)).toArray(SmsEnum.Entry[]::new)));
            topicList.setAdapter(new ArrayAdapter(activity, android.R.layout.simple_spinner_dropdown_item, Arrays.stream(TopicEnum.values()).map(item -> item.new Entry(activity)).toArray(TopicEnum.Entry[]::new)));
        }

        addRuleBtn.setOnClickListener(v -> {

            String type = smsItemsList.getSelectedItem().toString();
            String topic = topicList.getSelectedItem().toString();

            List<Rule> list = repository.list(SmsEnum.get(type).name(), TopicEnum.get(topic).name(), itemEditText.getText().toString(), toEditText.getText().toString());

            if (list.size() == 0) {

                long added = repository.add(new Rule(SmsEnum.get(type).name(), TopicEnum.get(topic).name(), itemEditText.getText().toString(), toEditText.getText().toString()));
                if (added > 0) {
                    customDialog.dismiss();
                } else {
                    logMessage.setText(activity.getResources().getText(R.string.not_added));
                }
            } else {
                logMessage.setText(activity.getResources().getText(R.string.already_added));
            }

        });

        selectPersons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectPersonDialog dialog = new SelectPersonDialog(activity, AddRuleDialog.this);
                dialog.show();
            }
        });
    }

    public static <T extends Enum<T>> T getEnumFromString(Class<T> c, String string) {
        if (c != null && string != null) {
            try {
                return Enum.valueOf(c, string.trim().toUpperCase());
            } catch (IllegalArgumentException ex) {
            }
        }
        return null;
    }

    private void initializeUI(View customView) {
        addRuleBtn = customView.findViewById(R.id.add_rule);
        logMessage = customView.findViewById(R.id.logMessage);
        name = customView.findViewById(R.id.name);
        message = customView.findViewById(R.id.message);
        smsItemsList = customView.findViewById(R.id.smsItemsList);
        topicList = customView.findViewById(R.id.topicList);
        itemEditText = customView.findViewById(R.id.item);
        toEditText = customView.findViewById(R.id.toEditText);
        selectPersons = customView.findViewById(R.id.selectFromPersons);
    }

    public void show() {
        customDialog.show();
    }

    @Override
    public void onSelected(Contact contact) {
        toEditText.setText(contact.getNumber());
    }


    public interface IDialog {
        void onPositive(Sms sms);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
