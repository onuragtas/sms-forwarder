package org.resoft.smsforwarder.ui.notifications;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.resoft.smsforwarder.DataManager;
import org.resoft.smsforwarder.Database;
import org.resoft.smsforwarder.R;
import org.resoft.smsforwarder.adapters.MessagesAdapter;
import org.resoft.smsforwarder.adapters.RulesAdapter;
import org.resoft.smsforwarder.entities.Rule;
import org.resoft.smsforwarder.models.Sms;
import org.resoft.smsforwarder.providers.SmsProvider;
import org.resoft.smsforwarder.repositories.RuleRepository;
import org.resoft.smsforwarder.ui.home.HomeFragment;
import org.resoft.smsforwarder.utils.Dialogs;

import java.util.ArrayList;
import java.util.List;

public class NotificationsFragment extends Fragment implements RulesAdapter.OnItemClickListener, Dialogs.IDialog {

    private List<Rule> list;
    private RulesAdapter adapter;
    private RuleRepository repository;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_notifications, container, false);

        repository = Database.getDatabase(getContext()).rule();

        RecyclerView recyclerView = root.findViewById(R.id.list);

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);

        DataManager.activity = getActivity();

        adapter = new RulesAdapter(NotificationsFragment.this, list);

        recyclerView.setAdapter(adapter);

        Handler handler = new Handler();
        RuleList threadSmslist = new RuleList(handler);
        threadSmslist.start();


        return root;
    }

    @Override
    public void onItemClick(Rule item) {

    }

    @Override
    public void OnLongItemClick(Rule item) {
        Dialogs.makeQuestionDialog(
                getActivity(),
                getResources().getText(R.string.areYouSureForDelete).toString(),
                getResources().getText(R.string.areYouSureForDelete).toString(),
                true,
                getResources().getText(R.string.yes).toString(),
                getResources().getText(R.string.no).toString(),
                this,
                item
        );
    }

    @Override
    public void onPositive(Object object) {
        repository.delete(((Rule)object).getId());

        RuleList threadSmslist = new RuleList(new Handler());
        threadSmslist.start();
    }

    @Override
    public void onNegative() {

    }

    @Override
    public void onCallback(ProgressDialog pd) {

    }


    class RuleList extends Thread {
        private final Handler handler;
        private final RuleRepository repository;

        public RuleList(Handler handler){
            this.handler = handler;
            this.repository = Database.getDatabase(getActivity()).rule();
        }

        @Override
        public void run() {
            super.run();
            handler.post(() -> adapter.setData(repository.list()));

        }
    }
}