package org.resoft.smsforwarder.ui.dashboard;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.resoft.smsforwarder.Database;
import org.resoft.smsforwarder.R;
import org.resoft.smsforwarder.adapters.ForwardsAdapter;
import org.resoft.smsforwarder.adapters.MessagesAdapter;
import org.resoft.smsforwarder.entities.Forward;
import org.resoft.smsforwarder.models.Sms;
import org.resoft.smsforwarder.providers.SmsProvider;
import org.resoft.smsforwarder.repositories.ForwardRepository;
import org.resoft.smsforwarder.repositories.RuleRepository;
import org.resoft.smsforwarder.ui.home.HomeFragment;

import java.util.ArrayList;
import java.util.List;

public class DashboardFragment extends Fragment implements ForwardsAdapter.OnItemClickListener {

    private DashboardViewModel dashboardViewModel;

    private List<Forward> messageList;
    private ForwardsAdapter adapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                new ViewModelProvider(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);

        RecyclerView recyclerView = root.findViewById(R.id.list);

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);

        adapter = new ForwardsAdapter(DashboardFragment.this, messageList);

        recyclerView.setAdapter(adapter);

        Handler handler = new Handler();
        ForwardList threadSmslist = new ForwardList(handler);
        threadSmslist.start();

        return root;
    }

    @Override
    public void onItemClick(Forward item) {

    }

    class ForwardList extends Thread {
        private final Handler handler;
        private final ForwardRepository repository;

        public ForwardList(Handler handler){
            this.handler = handler;
            this.repository = Database.getDatabase(getActivity()).forward();
        }

        @Override
        public void run() {
            super.run();
            handler.post(() -> adapter.setData(repository.list()));

        }
    }
}