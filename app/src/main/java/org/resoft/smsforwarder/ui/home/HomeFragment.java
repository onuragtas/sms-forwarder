package org.resoft.smsforwarder.ui.home;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.resoft.smsforwarder.DataManager;
import org.resoft.smsforwarder.MainActivity;
import org.resoft.smsforwarder.R;
import org.resoft.smsforwarder.adapters.MessagesAdapter;
import org.resoft.smsforwarder.models.Sms;
import org.resoft.smsforwarder.providers.SmsProvider;
import org.resoft.smsforwarder.ui.AddRuleDialog;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment implements MessagesAdapter.OnItemClickListener, AddRuleDialog.IDialog {

    private List<Sms> messageList;
    private MessagesAdapter adapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);

        RecyclerView recyclerView = root.findViewById(R.id.list);

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);

        messageList = DataManager.smsList;

        adapter = new MessagesAdapter(HomeFragment.this, messageList);

        recyclerView.setAdapter(adapter);

        Handler handler = new Handler();
        SmsList threadSmslist = new SmsList(handler);
        threadSmslist.start();

        return root;
    }

    public void makeDialog(){
        Dialog customDialog;
        LayoutInflater inflater = (LayoutInflater) getLayoutInflater();
        View customView = inflater.inflate(R.layout.add_rule_dialog, null);

        customDialog = new Dialog(getActivity(), R.style.WideDialog);

        getActivity().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);

        customDialog.setContentView(customView);
        customDialog.show();
    }

    @Override
    public void onItemClick(Sms sms) {
        DataManager.activity = getActivity();
        AddRuleDialog dialog = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            dialog = new AddRuleDialog(getActivity(), this, sms);
            dialog.show();
        }
    }

    @Override
    public void onPositive(Sms sms) {
        System.out.println(sms);
    }

    class SmsList extends Thread {
        private final Handler handler;

        public SmsList(Handler handler){
            this.handler = handler;
        }

        @Override
        public void run() {
            super.run();
            SmsProvider provider = new SmsProvider(getContext());

            provider.setOrder("_id");
            provider.setOrderType("DESC");
            provider.setUri("content://sms/conversations/");
            provider.setGroup("thread_id");

            ArrayList<Sms> smsList = new ArrayList<>();
            ArrayList<Sms> data = provider.query();

            for (Sms item:data) {
                if (!item.getAddress().equals("") && !item.getBody().equals("")){
                    smsList.add(item);
                }
            }

            handler.post(() -> adapter.setData(smsList));

        }
    }

}