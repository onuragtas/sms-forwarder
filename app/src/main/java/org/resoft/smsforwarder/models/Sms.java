package org.resoft.smsforwarder.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "smses")
public class Sms {

    @PrimaryKey(autoGenerate = true)
    private Long id;

    @ColumnInfo(name = "sms_id")
    private String sms_id = null;

    @ColumnInfo(name = "thread_id")
    private String thread_id = null;

    @ColumnInfo(name = "address")
    private String address = null;

    @ColumnInfo(name = "person")
    private String person = null;

    @ColumnInfo(name = "date")
    private String date = null;

    @ColumnInfo(name = "read_value")
    private String read_value = null;

    @ColumnInfo(name = "status")
    private String status = null;

    @ColumnInfo(name = "type")
    private String type = null;

    @ColumnInfo(name = "subject")
    private String subject = null;

    @ColumnInfo(name = "body")
    private String body = null;

    @ColumnInfo(name = "seen")
    private String seen = null;

    private String fromuser = null;

    public Sms(String sms_id, String thread_id, String address, String person, String date, String read_value, String status, String type, String subject, String body, String seen, String fromuser) {
        this.sms_id = sms_id;
        this.thread_id = thread_id;
        this.address = address;
        this.person = person;
        this.date = date;
        this.read_value = read_value;
        this.status = status;
        this.type = type;
        this.subject = subject;
        this.body = body;
        this.seen = seen;
        this.fromuser = fromuser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSms_id() {
        return sms_id;
    }

    public void setSms_id(String sms_id) {
        this.sms_id = sms_id;
    }

    public String getThread_id() {
        return thread_id;
    }

    public void setThread_id(String thread_id) {
        this.thread_id = thread_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRead_value() {
        return read_value;
    }

    public void setRead_value(String read_value) {
        this.read_value = read_value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSeen() {
        return seen;
    }

    public void setSeen(String seen) {
        this.seen = seen;
    }

    public String getFromuser() {
        return fromuser;
    }

    public void setFromuser(String fromuser) {
        this.fromuser = fromuser;
    }
}
