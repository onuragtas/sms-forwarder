package org.resoft.smsforwarder.enums;

import android.content.Context;

import androidx.annotation.StringRes;

import org.resoft.smsforwarder.DataManager;
import org.resoft.smsforwarder.R;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import xdroid.enumformat.EnumString;

public enum SmsEnum {
//    @EnumString(R.string.address)
    ADDRESS(R.string.address),

//    @EnumString(R.string.content)
    CONTENT(R.string.content);


    SmsEnum(@StringRes int label) {
        mLabel = label;
    }

    private @StringRes
    int mLabel;

    public class Entry {
        private final Context mContext;

        public Entry(final Context context) {
            mContext = context;
        }

        @Override
        public String toString() {
            return mContext.getString(mLabel);
        }
    }

    private static final Map<String,SmsEnum> ENUM_MAP;
    private static final Map<String,SmsEnum> ENUM_MAP_REVERSE;

    static {
        Map<String,SmsEnum> map = new ConcurrentHashMap<String, SmsEnum>();
        Map<String,SmsEnum> mapReverse = new ConcurrentHashMap<String, SmsEnum>();
        for (SmsEnum instance : SmsEnum.values()) {
            Context c;
            if (DataManager.activity == null){
                c = DataManager.context;
            }else{
                c = DataManager.activity.getApplicationContext();
            }
            map.put(instance.new Entry(c).toString(),instance);
            mapReverse.put(instance.name(),instance);
        }
        ENUM_MAP = Collections.unmodifiableMap(map);
        ENUM_MAP_REVERSE = Collections.unmodifiableMap(mapReverse);
    }

    public static SmsEnum get (String name) {
        return ENUM_MAP.get(name);
    }

    public static SmsEnum getReverse (String name) {
        return ENUM_MAP_REVERSE.get(name);
    }
}