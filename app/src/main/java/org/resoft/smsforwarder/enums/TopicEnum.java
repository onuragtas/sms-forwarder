package org.resoft.smsforwarder.enums;

import android.content.Context;

import androidx.annotation.StringRes;

import org.resoft.smsforwarder.DataManager;
import org.resoft.smsforwarder.R;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import xdroid.enumformat.EnumString;

public enum TopicEnum {

    DOES_CONTAIN_TOPIC(R.string.content_topic_does_contain),
    DOES_NOT_CONTAIN_TOPIC(R.string.content_topic_does_not_contain),
    BEGIN_CONTENT_TOPIC(R.string.content_topic_begin),
    EQUAL_CONTENT_TOPIC(R.string.content_topic_equal),
    NOT_EQUAL_CONTENT_TOPIC(R.string.content_topic_not_equal);

    TopicEnum(@StringRes int label) {
        mLabel = label;
    }

    private @StringRes
    int mLabel;

    public class Entry {
        private final Context mContext;

        public Entry(final Context context) {
            mContext = context;
        }

        @Override
        public String toString() {
            return mContext.getString(mLabel);
        }
    }

    private static final Map<String, TopicEnum> ENUM_MAP;
    private static final Map<String, TopicEnum> ENUM_MAP_REVERSE;

    static {
        Map<String, TopicEnum> map = new ConcurrentHashMap<String, TopicEnum>();
        Map<String, TopicEnum> mapReverse = new ConcurrentHashMap<String, TopicEnum>();
        for (TopicEnum instance : TopicEnum.values()) {

            Context c;
            if (DataManager.activity == null){
                c = DataManager.context;
            }else{
                c = DataManager.activity.getApplicationContext();
            }

            map.put(instance.new Entry(c).toString(), instance);
            mapReverse.put(instance.name(), instance);
        }
        ENUM_MAP = Collections.unmodifiableMap(map);
        ENUM_MAP_REVERSE = Collections.unmodifiableMap(mapReverse);
    }

    public static TopicEnum get(String name) {
        return ENUM_MAP.get(name);
    }

    public static TopicEnum getReverse(String name) {
        return ENUM_MAP_REVERSE.get(name);
    }
}